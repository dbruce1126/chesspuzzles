'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    Puzzle = mongoose.model('Puzzle'),
    uuid = require('node-uuid'),
    _ = require('lodash');

/**
 * Show the current article
 */
exports.read = function(req, res, next) {
    return res.json(req.puzzle);
};

/**
 * Show the current article
 */
exports.readRandom = function(req, res, next) {
    Puzzle.findOneRandom(function(err, element) {
        if (err) next(err);
        else res.json(element);
    });
};

exports.save = function(req, res, next) {
    var puzzle = new Puzzle();
    puzzle.initialFEN = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1';
    puzzle.validMoves = ['e4', 'e5', 'Nf3', 'Nc6', 'd4'];
    puzzle.save(function (err) {
        if (err) return next(err);
        res.json(puzzle);
    });
};

/**
 * Retrieve a puzzle by its ID
 */
exports.puzzleById = function(req, res, next, puzzleId) {
    Puzzle.findById(puzzleId, function(err, puzzle) {
        if (err) return next(err);
        req.puzzle = puzzle;
        next();
    });
};