'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    random = require('mongoose-simple-random');

/**
 * Puzzle Schema
 */
var PuzzleSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    initialFEN: {
        type: String,
        required: 'Missing initial FEN string.'
    },
    validMoves: [
        {
            type: String,
            lowercase: true,
            trim: true
        }
    ]
});
PuzzleSchema.plugin(random);

mongoose.model('Puzzle', PuzzleSchema);