'use strict';

var puzzle = require('../../app/controllers/puzzles.server.controller');

module.exports = function(app) {
    app.route('/api/puzzle/random').get(puzzle.readRandom);
    app.route('/api/puzzle/save').get(puzzle.save);
    app.route('/api/puzzle/:puzzleId').get(puzzle.read);

    app.param('puzzleId', puzzle.puzzleById);
};